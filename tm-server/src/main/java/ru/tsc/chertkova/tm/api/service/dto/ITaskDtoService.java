package ru.tsc.chertkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.tsc.chertkova.tm.dto.model.TaskDTO;
import ru.tsc.chertkova.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnerDtoService<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId);

    @Nullable
    TaskDTO add(@Nullable TaskDTO task);

    @Nullable
    TaskDTO updateById(
            @Nullable String id,
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description);

    @Nullable
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status);

    boolean existsById(@Nullable String id);

    @Nullable
    TaskDTO findById(@Nullable String userId,
                     @Nullable String id);

    TaskDTO removeById(@Nullable String userId,
                       @Nullable String id);

    TaskDTO remove(@Nullable String userId,
                   @Nullable TaskDTO task);

    int getSize(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> addAll(@NotNull List<TaskDTO> tasks);

    @Nullable
    List<TaskDTO> removeAll(@Nullable List<TaskDTO> tasks);

}
