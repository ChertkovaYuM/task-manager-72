package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.service.dto.ITaskDtoService;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.StatusNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.DescriptionEmptyException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.repository.dto.TaskDtoRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TaskDtoService extends AbstractUserOwnerDtoService<TaskDTO> implements ITaskDtoService {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId,
                                            @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @Nullable List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
        return tasks;
    }

    @Override
    @Nullable
    @Transactional
    public TaskDTO add(@Nullable final TaskDTO task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUserId()).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(task.getName()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(task.getProjectId()).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(task.getUserId()).orElseThrow(TaskNotFoundException::new);
        taskRepository.saveAndFlush(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @Nullable TaskDTO task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.saveAndFlush(task);
        task = findById(userId, id);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusById(@Nullable final String userId,
                                        @Nullable final String id,
                                        @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @Nullable TaskDTO task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.changeStatus(id, userId, status.getDisplayName());
        task = findById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return taskRepository.existsById(id);
    }

    @Override
    @Transactional
    public @Nullable TaskDTO findById(@Nullable final String userId,
                                      @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable TaskDTO task = taskRepository.findById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public TaskDTO removeById(@Nullable final String userId,
                              @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final TaskDTO task = findById(userId, id);
        taskRepository.removeById(userId, id);
        return task;
    }

    @Override
    @Transactional
    public TaskDTO remove(@Nullable final String userId,
                          @Nullable final TaskDTO task) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        Optional.ofNullable(findById(task.getUserId(), task.getId()))
                .orElseThrow(TaskNotFoundException::new);
        removeById(task.getUserId(), task.getId());
        return task;
    }

    @Override
    @Transactional
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        int size = taskRepository.getSize(userId);
        return size;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        taskRepository.clear(userId);
    }

    @Override
    @Nullable
    @Transactional
    public List<TaskDTO> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @Nullable List<TaskDTO> tasks = taskRepository.findAll(userId);
        return tasks;
    }

    @Override
    @Nullable
    @Transactional
    public List<TaskDTO> addAll(@NotNull final List<TaskDTO> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        taskRepository.saveAll(tasks);
        return tasks;
    }

    @Override
    @Nullable
    @Transactional
    public List<TaskDTO> removeAll(@Nullable final List<TaskDTO> tasks) {
        Optional.ofNullable(tasks).orElseThrow(ProjectNotFoundException::new);
        taskRepository.deleteAll(tasks);
        return tasks;
    }

}
