package ru.tsc.chertkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractTaskListener;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getTaskEndpoint().completeTaskStatusById(new TaskCompleteByIdRequest(getToken(), id))
                .getTask().setDateEnd(new Date());
    }

}
