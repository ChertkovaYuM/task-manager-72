package ru.tsc.chertkova.tm.exception.entity;

import ru.tsc.chertkova.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
