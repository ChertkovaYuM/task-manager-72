package ru.tsc.chertkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String projectId;

    public TaskCreateRequest(@Nullable String token,
                             @Nullable String name,
                             @Nullable String description,
                             @Nullable String projectId) {
        super(token);
        this.name = name;
        this.description = description;
        this.projectId=projectId;
    }

}
