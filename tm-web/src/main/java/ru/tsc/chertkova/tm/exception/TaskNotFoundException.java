package ru.tsc.chertkova.tm.exception;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Ошибка! Задача не найдена!");
    }

}
