package ru.tsc.chertkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @PutMapping("/create")
    void create();

    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    ProjectDTO save(
            @NotNull
            @WebParam(name = "ProjectDTO")
            @RequestBody ProjectDTO ProjectDTO
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "ProjectDTO")
            @RequestBody ProjectDTO ProjectDTO
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "ProjectDTOs")
            @RequestBody List<ProjectDTO> ProjectDTOs
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

}
