package ru.tsc.chertkova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;
import ru.tsc.chertkova.tm.service.ProjectService;
import ru.tsc.chertkova.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Nullable
    private Collection<ProjectDTO> getProjects() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @GetMapping("/project/create")
    public String create() {
        projectService.create(UserUtil.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectService.deleteById(UserUtil.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") ProjectDTO project, BindingResult result) {
        projectService.save(UserUtil.getUserId(), project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final ProjectDTO project = projectService.findById(UserUtil.getUserId(), id);
        return new ModelAndView("project-edit", "project", project);
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    @GetMapping("/projects")
    public ModelAndView list() {
        return new ModelAndView("project-list", "projects", getProjects());
    }

}
