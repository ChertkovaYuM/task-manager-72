<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp">
 <jsp:param name="title" value="Projects"/>
</jsp:include>
<h1>Projects</h1>
<table>
    <tr>
        <th>#</th>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Description</th>
        <th>Created</th>
        <th>Started</th>
        <th>Completed</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}" varStatus="status">
        <tr>
            <td>${status.index + 1}</td>
            <td>${project.id}</td>
            <td>${project.name}</td>
            <td>${project.status.displayName}</td>
            <td>${project.description}</td>
            <td>
                <fmt:formatDate value="${project.created}" pattern="yyyy-MM-dd HH:mm"/>
            </td>
            <td>
                <fmt:formatDate value="${project.dateBegin}" pattern="yyyy-MM-dd HH:mm"/>
            </td>
            <td>
                <fmt:formatDate value="${project.dateEnd}" pattern="yyyy-MM-dd HH:mm"/>
            </td>
            <td>
                <form:form class="mini" method="get" action="/project/edit/${project.id}">
                    <button type="submit">Edit</button>
                </form:form>
            </td>
            <td>
                <form:form class="mini" method="get" action="/project/delete/${project.id}">
                    <button type="submit">Delete</button>
                </form:form>
            </td>
        </tr>
    </c:forEach>
</table>
<form:form method="get" action="/project/create">
    <button type="submit">Create</button>
</form:form>
<jsp:include page="../include/_footer.jsp" />